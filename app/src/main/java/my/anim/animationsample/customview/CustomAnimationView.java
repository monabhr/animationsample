package my.anim.animationsample.customview;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;

public class CustomAnimationView extends View {

    private float mRadius;
    private final Paint mPaint = new Paint();
    private static final int COLOR_ADJUSTER = 5;

    //store the event coordinates
    private float mX;
    private float mY;

    //The animation is performed by an Animator object that, once started, changes the value of a property from a starting value towards an end value over a given duration
    private static final int ANIMATION_DURATION = 4000;
    private static final long ANIMATION_DELAY = 1000;

    private AnimatorSet animatorSet = new AnimatorSet();

    public CustomAnimationView(Context context) {
        this(context, null);
    }

    public CustomAnimationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setRadius(float radius) {
        this.mRadius = radius;
        mPaint.setColor(Color.GREEN + (int) radius / COLOR_ADJUSTER);

        // Updating the property does not automatically redraw.
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            mX = event.getX();
            mY = event.getY();

            if(animatorSet != null && animatorSet.isRunning()){
                animatorSet.cancel();
            }
            animatorSet.start();
        }



        return super.onTouchEvent(event);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        //name of the property that is to be animated => "radius"
        ObjectAnimator growAnimator = ObjectAnimator.ofFloat(this,
                "radius", 0, getWidth());

        //The interpolator affects the rate of change; that is, the interpolator affects how the animated property changes from its starting value to its ending value.
        growAnimator.setInterpolator(new LinearInterpolator());
        growAnimator.setDuration(ANIMATION_DURATION);

        ObjectAnimator shrinkAnimator = ObjectAnimator.ofFloat(this,
                "radius", getWidth(), 0);
        shrinkAnimator.setDuration(ANIMATION_DURATION);
        shrinkAnimator.setInterpolator(new LinearOutSlowInInterpolator());


        ObjectAnimator repeatAnimator = ObjectAnimator.ofFloat(this,
                "radius", 0, getWidth());
        repeatAnimator.setStartDelay(ANIMATION_DELAY);
        repeatAnimator.setDuration(ANIMATION_DURATION);
        repeatAnimator.setRepeatCount(1);
        //every time the animation plays, it reverses the beginning and end values. (The other possible value, which is the default, is RESTART
        repeatAnimator.setRepeatMode(ValueAnimator.REVERSE);

        animatorSet.play(growAnimator).before(shrinkAnimator);
        animatorSet.play(repeatAnimator).after(shrinkAnimator);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(mX, mY, mRadius, mPaint);
    }
}
